import bs4 as bs
from urllib.request import urlopen
import urllib
import re
import logging
import requests
import sys
import os

logging.basicConfig(filename='chupaCabra.log',level=logging.DEBUG,format='%(asctime)s %(message)s')
logging.info('Início da operação')


#connect to a URL
website = urlopen("https://www.conectacompras.com.br/")

#read html code
html = website.read().decode('ISO-8859-1')

soup = bs.BeautifulSoup(html,'lxml')

listaCategoria = soup.find_all('a',class_="new_sub_menu")
for categoria in listaCategoria:      
    try:
        linkCategoria = categoria.get('href')
        print(linkCategoria)
        incioDescCate = linkCategoria.find("&")
        inicioCodCat = linkCategoria.find("=")
        id_sub = linkCategoria[inicioCodCat+1:incioDescCate]    
        descSub = linkCategoria[incioDescCate+1:]
        logmessage = "Chupando Categoria {0} - Qtde:{1}  id:{2}".format(descSub,len(listaCategoria),id_sub)
        logging.info(logmessage)
        #Pega todos produtos da categoria
        paginaCategoria = urlopen("https://www.conectacompras.com.br/produtos.php?id_sub={0}&pageNum=VER-TUDO".format(id_sub))
        html = paginaCategoria.read().decode('ISO-8859-1')
        
        soupCategoria = bs.BeautifulSoup(html,'lxml')
        for produtos in soupCategoria.find_all('div',class_='item-meta-container'):
            linkProduto = produtos.h3.a.get('href')
            #"produto.php?id_prod=47870&agua-mineral-bonafont-natural-500ml-"
            inicioCodigo = linkProduto.find("=")
            fimCodigo = linkProduto.find("&")
            codigoProduto = linkProduto[inicioCodigo+1:fimCodigo]
            descricaoProduto = produtos.h3.a.string        
            codigoBarra = re.search(r'\d{1,}',produtos.div.find('div',class_='ratings-container').small.a.text)
            imagem = soupCategoria.find('a',href=linkProduto).img.get('src')
            linkImagem = "https://www.conectacompras.com.br/"+imagem
            logmessage = "Chupando Produto "+descricaoProduto+" - EAN:"+codigoBarra.group(0)+" id:"+codigoProduto
            logging.info(logmessage)
            print(codigoBarra.group(0))
            print(descricaoProduto)      
            print(linkImagem)        
            filename = ".\\imagem\\{0}.png".format(codigoBarra.group(0))   
            #Se o arquivo existe entao ja enviou 
            if os.path.isfile(filename):
              logging.info("Produto já existe "+codigoBarra.group(0))
              continue     

            urllib.request.urlretrieve(linkImagem,filename)
            logging.info("Imagem salva {0}.jpg".format(codigoBarra))
            logging.info("Enviando dados do produto {0} para o site".format(codigoBarra))
            data = {
                "codigo":codigoProduto,
                "descricao":descricaoProduto,
                "ean13":codigoBarra.group(0),
                "categoria":{
                    "descricao":descSub,
                    "codigo":id_sub
                }
            }                
            response = requests.post("https://ean13.herokuapp.com//produtos", json=data)        
            if response.status_code == 201:
                logging.info("Produto enviado com sucesso")            
                up = {'file':(filename, open(filename, 'rb'), "multipart/form-data")}

                response = requests.post("https://ean13.herokuapp.com/produtos/{0}/foto".format(codigoBarra.group(0)),files=up)
                if response.status_code == 200:
                    logging.info("Imagem enviada com sucesso")
            else:
                logging.error("Erro ao enviar produto")        
    except:
        logging.error(sys.exc_info()[0])        












